# CS 4488 - Senior Design Project
## Please update this accordingly. I'm trying to write documentation as I go.
[How to update this document](https://help.github.com/articles/basic-writing-and-formatting-syntax/)

## Team members
- Brandon Biggs Project Director
- Tristen Potter (TJ) Communication/Game, Lead
- Mark Murdock Communication/Game
- Andrew Perot Communication/Game
- Jacob Lehmer Graphical/User Interface, Lead
- Andrew Spooner (Spooner) Graphical/User Interface
- Justin Tennant Systems, Lead
- Justin Donaldson Systems
- Ryan Rickenbach Documentation Writer, Systems

## Running the app locally
I've tried to make this fairly easy to get running locally, but you'll have to do a few things. These are all via terminal
commands. I'm not sure how to use git via windows or with a GUI, but I'm sure there are a lot of options.
1. Make sure that node.js is installed on your computer - [Node.js](https://nodejs.org/en/)
2. Clone our directory `git clone git@gitlab.com:CS4488-Pocatello/SeniorDesignAsteroids.git`
3. Run `node index.js`. This will start the local webserver at localhost:1337

## Making your own changes
I think we should avoid pushing to master until we verify that there aren't any immediately noticeable bugs. 
To create your own branch and make changes that you'd like tracked by git, do the following.
1. `git fetch`
2. `git checkout -b [name of your branch here]`
3. make your changes and test them on your local node installation
4. `git add .` // this adds files to manifest to be tracked
5. `git commit -a -m "[reason for commit/description]"`
6. a) If you've created a new branch
`git push --set-upstream origin [branch name]`
6. b) If you haven't created a new branch `git push`