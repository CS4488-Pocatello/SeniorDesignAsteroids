// Brandon Biggs 20180227
// NODE FILE
var gameSetup = {

    setupGame: function (io, server) {

        var asteroids = [];
        var projectiles = [];
        var pickups = [];
        var goldenGun = false;
        
        // sprites array will contain all server side sprites that can
        // be sent at once inside server tick. Still needs to have a
        // method that will get and push all players/projectiles/ast.
        // to it to fill it up on call.
        //var sprites = [];

        var timeout;
        //var changed = false;
        //var changeCount = 0;

        function serverTick() {

            io.emit('updatePlayers', getAlivePlayers());

            //2018-04-01 Jacob Lehmer
            //This is where the collisions between projectiles and asteroids and projectiles and players
            projectiles.forEach(function (projectile) {
                //This represents vT
                var projvI = Math.cos(projectile.rot);
                var projvJ = Math.sin(projectile.rot);
                var projrI = projectile.x;
                var projrJ = projectile.y;
                var projI = projrI + projvI;
                var projJ = projrJ + projvJ;
                asteroids.forEach(function (asteroid) {
                    var astvI = asteroid.dx;
                    var astvJ = asteroid.dy;
                    var astrI = asteroid.x;
                    var astrJ = asteroid.y;

                    var combinedRadius = projectile.radius + asteroid.radius;
                    var astI = astrI + astvI;
                    var astJ = astrJ + astvJ;

                    var relI = projI - astI;
                    var relJ = projJ - astJ;
                    if ((relI * relI + relJ * relJ) <= combinedRadius * combinedRadius) {
                        //Edited by Ryan Rickenbach 2018-04-10 adds points to player when hitting an asteroid. If we want we can change it so it changes based on asteroid size.
                        //THIS IS WHERE A COLLISION IS REGISTERED for a projectile and asteroid
                        //console.log("Collision Between Projectile "+ projectile.id + " parent ID: " + projectile.parent +" and Asteroid ID "+asteroid.id);
                        getAlivePlayers().forEach(function (player) {
                            if (asteroid.scale == 0.4) {
                                if (projectile.parent.id == player.id) {
                                    player.score += 10;
                                    io.emit('score updated', {
                                        username: player.username,
                                        score: player.score,
                                        playerID: player.id
                                    });
                                }
                            }
                            if (asteroid.scale == 0.3) {
                                if (projectile.parent.id == player.id) {
                                    player.score += 15;
                                    io.emit('score updated', {
                                        username: player.username,
                                        score: player.score,
                                        playerID: player.id
                                    });
                                }
                            }
                            if (asteroid.scale == 0.2) {
                                if (projectile.parent.id == player.id) {
                                    player.score += 20;
                                    io.emit('score updated', {
                                        username: player.username,
                                        score: player.score,
                                        playerID: player.id
                                    });
                                }
                            }
                            if (asteroid.scale == 0.1) {
                                if (projectile.parent.id == player.id) {
                                    player.score += 25;
                                    io.emit('score updated', {
                                        username: player.username,
                                        score: player.score,
                                        playerID: player.id
                                    });
                                }
                            }
                        });

                        // Testing, removing asteroid on projectile collision
                        // - Justin Tennant, 4.04.18
                        io.emit('removeAsteroid', asteroid.id);
                        asteroid.toRemove = true;
                        projectile.toRemove = true;
                        cleanAsteroids();

                        // Creates smaller asteroid debris when asteroids
                        // of various sizes are destroyed.
                        // Andrew Perot 4/6/18
                        var pUp = randomInt(0,4);
                        if(asteroid.scale == 0.4){
                            for(var i=0; i<3; i++){
                                spawnAsteroid(asteroid.x,asteroid.y,0.3)
                            }
                            if(pUp == 1){
                                spawnPickup(asteroid.x,asteroid.y,0.07)
                            }
                        }
                        if(asteroid.scale == 0.3){
                            for(var i=0; i<3; i++){
                                spawnAsteroid(asteroid.x,asteroid.y,0.2)
                            }
                            if(pUp == 2){
                                spawnPickup(asteroid.x,asteroid.y,0.05)
                            }
                        }
                        else if(asteroid.scale == 0.2){
                            for(var i=0; i<3; i++){
                                spawnAsteroid(asteroid.x,asteroid.y,0.1)
                            }
                            if(pUp == 3){
                                spawnPickup(asteroid.x,asteroid.y,0.03)
                            }
                        }

                        //THIS IS WHERE A COLLISION IS REGISTERED for a projectile and asteroid
                    }

                    // Replenishes the asteroids on the map when their number dips below a given amount.
                    // Andrew Perot 4/6/18
                    if (asteroids.length < 50) {
                        spawnAsteroid(randomInt(-200, 200), randomInt(-200, 200), randomInt(1, 4) / 10)
                    }
                });

                getAlivePlayers().forEach(function (player) {

                    var plyrvI = 0;//player.dx;
                    var plyrvJ = 0;//player.dy;
                    var plyrrI = player.x;
                    var plyrrJ = player.y;

                    var combinedRadius = projectile.radius + player.radius;
                    var plyrI = plyrrI + plyrvI;
                    var plyrJ = plyrrJ + plyrvJ;

                    var relI = projI - plyrI;
                    var relJ = projJ - plyrJ;
                    if ((relI * relI + relJ * relJ) <= combinedRadius * combinedRadius) {
                        //THIS IS WHERE A COLLISION IS REGISTERED for a projectile and player
                        //console.log("Collision Between Projectile "+ projectile.id +" and Player ID "+player.id);

                        if (!player.invulnerable && player.age > 10 && projectile.parent != player) {
                            console.log(projectile.parent.username + " shot " + player.username);
                            if(goldenGun == true)
                                player.health -=100;
                            else
                                player.health-=25;
                            player.age = 0;
                            if (player.health <= 0)
                                projectile.parent.score += 100;
                            deadPlayerCheck();
                        }
                        //THIS IS WHERE A COLLISION IS REGISTERED for a projectile and player
                    }
                });
            });
            getAlivePlayers().forEach(function (player) {
                player.age++;
                var plyrvI = player.dx;
                var plyrvJ = player.dy;
                var plyrrI = player.x;
                var plyrrJ = player.y;
                var plyrI = plyrrI + plyrvI;
                var plyrJ = plyrrJ + plyrvJ;

                asteroids.forEach(function (asteroid) {
                    var astvI = asteroid.dx;
                    var astvJ = asteroid.dy;
                    var astrI = asteroid.x;
                    var astrJ = asteroid.y;

                    var combinedRadius = player.radius + asteroid.radius;
                    var astI = astrI + astvI;
                    var astJ = astrJ + astvJ;

                    var relI = plyrI - astI;
                    var relJ = plyrJ - astJ;
                    if ((relI * relI + relJ * relJ) <= combinedRadius * combinedRadius) {
                        ////Edited by Ryan Rickenbach 2018-04-10 Controls for taking damage
                        //THIS IS WHERE A COLLISION IS REGISTERED for a player and asteroid
                        //console.log("Collision Between Player "+ player.id +" and Asteroid ID "+asteroid.id);
                        //Ryan Rickenbach 2018-11-04
                        //Added the socket event however this will likely change so the client asks for the health
                        //rather than this system sending health when damage taken.
                        if (asteroid.scale == 0.4) {
                            if (!player.invulnerable && player.age > 10) {
                                player.health -= 25;
                                player.age = 0;
                                deadPlayerCheck();
                            }
                        }
                        if (asteroid.scale == 0.3) {
                            if (!player.invulnerable && player.age > 10) {
                                player.health -= 15;
                                player.age = 0;
                                deadPlayerCheck();
                            }
                        }
                        if (asteroid.scale == 0.2) {
                            if (!player.invulnerable && player.age > 10) {
                                player.health -= 10;
                                player.age = 0;
                                deadPlayerCheck();
                            }
                        }
                        if (asteroid.scale == 0.1) {
                            if (!player.invulnerable && player.age > 10) {
                                player.health -= 5;
                                player.age = 0;
                                deadPlayerCheck();
                            }
                        }

                        //THIS IS WHERE A COLLISION IS REGISTERED for a player and asteroid
                    }
                });
            });
            
            getAlivePlayers().forEach(function(player)
            {
                var plyrvI = 0;
                var plyrvJ = 0;
                var plyrrI = player.x;
                var plyrrJ = player.y;
                var plyrI = plyrrI +plyrvI;
                var plyrJ = plyrrJ + plyrvJ;

                pickups.forEach(function(pickup)
                {
                    var pickvI = pickup.dx;
                    var pickvJ = pickup.dy;
                    var pickrI = pickup.x;
                    var pickrJ = pickup.y;

                    var combinedRadius = player.radius + pickup.radius;
                    var pickI = pickrI + pickvI;
                    var pickJ = pickrJ + pickvJ;

                    var relI = plyrI - pickI;
                    var relJ = plyrJ - pickJ;
                    if((relI*relI + relJ*relJ )<= combinedRadius*combinedRadius)
                    {
                        if(pickup.scale == 0.07){
                            if(pickup.type == 4){
                                player.health += 20;
                            }
                            if(pickup.type == 5){
                                player.invulnerable = true;
                                setTimeout(setPlayerToVulnerable,15000);
                            }
                            if(pickup.type == 6){
                                goldenGun = true;
                                setTimeout(turnOffGoldenGun,15000);
                            }
                            if(pickup.type == 7){
                                player.x = randomInt(-200,200);
                                player.y = randomInt(-200,200);
                            }
                        }
                        if(pickup.scale == 0.05) {
                            if(pickup.type == 4){
                                player.health += 10;
                            }
                            if(pickup.type == 5){
                                player.invulnerable = true;
                                setTimeout(setPlayerToVulnerable,10000);
                            }
                            if(pickup.type == 6){
                                goldenGun = true;
                                setTimeout(turnOffGoldenGun,10000);
                            }
                            if(pickup.type == 7){
                                player.x = randomInt(-200,200);
                                player.y = randomInt(-200,200);
                            }
                        }
                        if(pickup.scale == 0.03){
                            if(pickup.type == 4){
                                player.health += 5;
                            }
                            if(pickup.type == 5){
                                player.invulnerable = true;
                                setTimeout(setPlayerToVulnerable,5000);
                            }
                            if(pickup.type == 6){
                                goldenGun = true;
                                setTimeout(turnOffGoldenGun,5000);
                            }
                            if(pickup.type == 7){
                                player.x = randomInt(-200,200);
                                player.y = randomInt(-200,200);
                            }
                        }
                        io.emit('removePickup',pickup.id);
                        pickup.toRemove = true;
                        cleanPickups();
                        function setPlayerToVulnerable() {
                            player.invulnerable = false;
                        }
                        function turnOffGoldenGun(){
                            goldenGun = false;
                        }
                    }
                });
            });
            
            getAlivePlayers().forEach(function (player) {
                player.x += player.dx;
                player.y += player.dy;
            });

            projectiles.forEach(function (element) {
                element.x += element.dx;
                element.y += element.dy;
                //console.log("X: " + element.x);

                element.age++;
                if (element.age > 150) {
                    element.toRemove = true;
                }
            });

            io.emit('updateProjectiles', getAllProjectiles());

            cleanProjectiles();

            asteroids.forEach(function (element) {
                element.x += element.dx;
                if (element.x < -200) {
                    element.x = 200;
                }
                if (element.x > 200) {
                    element.x = -200;
                }
                element.y += element.dy;
                if (element.y < -200) {
                    element.y = 200;
                }
                if (element.y > 200) {
                    element.y = -200;
                }
            });
            io.emit('updateAsteroids', getAllAsteroids());
            
            pickups.forEach(function(element){
                element.x += element.dx;
                if(element.x < -200){
                    element.x = 200;
                }
                if(element.x > 200){
                    element.x = -200;
                }
                element.y += element.dy;
                if(element.y < -200){
                    element.y = 200;
                }
                if(element.y > 200){
                    element.y = -200;
                }
            });
            io.emit('updatePickups',getAllPickups());

            //changed = false;
            //changeCount++;

            timeout = setTimeout(serverTick, 1000 / 40);
        }

        serverTick();

        io.on('connection', function (socket) {
            // Game player creation sockets
            // -Justin Tennant, 2-16-18
            socket.on('newplayer', function () {

                // Sprite data - Can be a player, projectile, astroid, etc..
                //Edited by Jacob Lehemr 2018-04-01 to add needed components for collisions
                //Edited by Ryan Rickenbach 2018-04-10 added invulnerable, score, health, age.
                socket.player = {
                    // Increment sprite ID on each creation
                    id: server.lastSpriteID++,
                    parent: null,
                    animation: false,
                    username: null,
                    new: false,
                    invulnerable: true,
                    age: 0,
                    type: 1,
                    radius: 1.8,
                    health: 100,
                    score: 0,
                    x: randomInt(-50, 50),
                    y: randomInt(-50, 50),
                    dx: 0,
                    dy: 0,
                    rot: 0,
                    toRemove: false
                };

                // Change flag on whether animation is running
                // Justin Tennant - 4.11.18
                socket.on('thrust', function (data) {
                    socket.player.animation = data;
                });

                // Socket should be received when player presses enter after
                // typing a username
                // - Justin Tennant, 4.11.18
                socket.on('add new user', function (username) {
                    socket.player.username = username;

                    // Removing invulnerability here allows player to be invincible
                    // until they have chosen a name
                    // - Justin Tennant, 4.11.18
                    invulnTimer(socket.player, 2000);
                })

                socket.on('get ID', function () {
                    io.emit('set playerID', socket.player.id);
                })

                //Ryan Rickenbach 2018-11-04
                //Game listens for client to ask for score on the timer. Right now it is done 1/10 a second but can change
                //Sends back the player username and the score for that player.
                socket.on('update scores', function () {

                    var playerArray = [];
                    getAlivePlayers().forEach(function (player) {
                        if (player.username != null) {
                            playerArray.push({username: player.username, score: player.score})
                        }
                        io.emit('score updated', playerArray);
                    });
                })

                socket.on('update health', function (data) {
                    var playerHealth;
                    getAlivePlayers().forEach(function (player) {
                        if (player.id == data) {
                            playerHealth = player.health;
                        }
                        io.emit('health updated', {
                            id: player.id,
                            health: player.health
                        });
                    });
                })

                // Server assigns player to connecting client
                socket.emit('thisplayer', socket.player);

                // Server updates connecting client list of current players
                socket.emit('allplayers', getAlivePlayers());

                // Debugging function, gets static projectile locations
                socket.emit('projectiles', getAllProjectiles());

                // Server tells all but the connecting client that new player joined
                socket.broadcast.emit('newplayer', socket.player);

                // Placeholder for handling movements and actions
                //Edited by Jacob Lehemr 2018-04-01 to add needed components for collisions
                socket.on('input', function (data) {
                    if (data.rot != 0) {
                        socket.player.rot += data.rot;
                        changed = true;
                    }
                    if (data.thrusting != 0) {
						var dxAdd = data.thrusting * (Math.cos(socket.player.rot));
						var dyAdd = data.thrusting * (Math.sin(socket.player.rot));
						
						// If player isn't going too fast, keep adding to dx
						if (Math.abs(socket.player.dx + dxAdd) <= 1){
							if (data.turbo)
								socket.player.dx += 1.5*dxAdd;
							else
								socket.player.dx += dxAdd;
						}
						else if (Math.abs(socket.player.dx + dxAdd) <= 2.5 && data.turbo){
							socket.player.dx += 1.5*dxAdd;
						}
						// If they are going too fast, let them slow down but not speed up
						else if (Math.abs(socket.player.dx + dxAdd) < Math.abs(socket.player.dx)){
							if (data.turbo)
								socket.player.dx += 1.5*dxAdd;
							else
								socket.player.dx += dxAdd;
						}
						// Repeat process for dy
						if (Math.abs(socket.player.dy + dyAdd) <= 1){
							if (data.turbo)
								socket.player.dy += 1.5*dyAdd;
							else
								socket.player.dy += dyAdd;
						}
						else if (Math.abs(socket.player.dy + dyAdd) <= 2.5 && data.turbo){
							socket.player.dy += 1.5*dyAdd;
						}
						else if (Math.abs(socket.player.dy + dyAdd) < Math.abs(socket.player.dy)){
							if (data.turbo)
								socket.player.dy += 1.5*dyAdd;
							else
								socket.player.dy += dyAdd;
						}
						changed = true;
                    }
                    if (data.fire === true) {
                        socket.projectile = {
                            id: server.lastSpriteID++,
                            parent: socket.player,
                            animation: true,
                            username: null,
                            new: true,
                            type: 3,
                            radius: 0.3,
                            x: socket.player.x,
                            y: socket.player.y,
                            dx: socket.player.dx + 1.5*Math.cos(socket.player.rot),
                            dy: socket.player.dy + 1.5*Math.sin(socket.player.rot),
                            rot: socket.player.rot,
                            age: 0,
                            toRemove: false
                        };
                        //console.log(socket.projectile.rot);
                        projectiles.push(socket.projectile);
                    }

                });

                socket.on('revivePlayer', function () {
                    socket.player.toRemove = false;
                    console.log("player " + socket.player.id + " revived");
                    io.emit('allplayers', getAlivePlayers());
                                        
                    invulnTimer(socket.player, 3000);
                });
                
                // Player destruction on exit or close browser
                socket.on('disconnect', function () {
                    io.emit('remove', socket.player.id);
                });
            });
        });

        // Creates the field of asteroids
        // Andrew Perot, 3/26/18
        //Edited by Jacob Lehemr 2018-04-01 to add needed components for collisions
        // Edited by Andrew Perot 4/6/18: Moved the code into a separate function that can be called several times.
        for (var i = 0; i < 50; i++) {
            spawnAsteroid(randomInt(-200, 200), randomInt(-200, 200), randomInt(1, 4) / 10)
        }

        // Not done
        // Justin Tennant 4.9.18
        function getAllSprites() {
            var sprites = [];

            cleanAsteroids();
            cleanProjectiles();
            var tempPlayers = getAlivePlayers();

            // For each element in Ast.Proj.Play, sprites.push(element) 
        }

        // Create array of all players. This is gathered by checking active sockets
        // so deletion is automatic upon player disconnect.
        // -Justin Tennant, 2-16-18
        function getAllPlayers() {
            var players = [];
            Object.keys(io.sockets.connected).forEach(function (socketID) {
                var player = io.sockets.connected[socketID].player;
                if (player) players.push(player);
            });
            return players;
        }
        
        function getAlivePlayers() {
            var players = [];
            Object.keys(io.sockets.connected).forEach(function (socketID) {
                var player = io.sockets.connected[socketID].player;
                if (player)
                    if(!player.toRemove) players.push(player);
            });
            return players;
        }

        // Resets player to random location on death. Modify death penalties here
        // - Justin Tennant, 4.12.18
        function deadPlayerCheck() {
            getAllPlayers().forEach(function (player) {
                if (player.health <= 0) {    
                    console.log(player.username + " has died.")
                    
                    player.toRemove = true;
                    player.x = randomInt(-50, 50);
                    player.y = randomInt(-50, 50);
                    player.dx = 0;
                    player.dy = 0;
                    player.health = 100;
                    player.score = 0;

                    // Emits to chat file to launch game over screen
                    io.emit('game over', player.id);
                    
                    // Emits to client file to remove player from screen
                    io.emit('player died', player.id);
                }
            });
        }

        // Sets player to invulnerable for x ms time
        function invulnTimer(player, x) {
            player.invulnerable = true;
            setTimeout(timerCountDown, x);

            function timerCountDown() {
                console.log(player.username + " is no longer invulnerable");
                player.invulnerable = false;
                player.age = 0;
            }
        }

        // Returns all asteroids
        function getAllAsteroids() {
            return asteroids;
        }

        // Removes instances of asteroids server side, doesn't affect
        // the client's listing object.
        // Justin Tennant 4.04.18
        function cleanAsteroids() {
            for (var i = 0; i < asteroids.length; i++) {
                if (asteroids[i].toRemove === true) {
                    asteroids.splice(i, 1);
                }
            }
        }

        // Creates instances of asteroids. Separate
        // function created to reduce code redundancy.
        // Andrew Perot 4/6/18
        function spawnAsteroid(xPos, yPos, rad) {
            var asteroid = {
                id: server.lastSpriteID++,
                parent: null,
                new: true,
                type: 2,
                radius: 15 * rad,
                x: xPos,
                y: yPos,
                scale: rad,
                rot: Math.random() * 6.0,
                dx: randomInt(-20, 20) / 100,
                dy: randomInt(-20, 20) / 100,
                toRemove: false
            };
            asteroids.push(asteroid);
        }
        
        function getAllPickups(){
            return pickups;
       }

        function cleanPickups(){
            for (var i = 0; i < pickups.length; i++)
            {
                if (pickups[i].toRemove === true)
                {
                    pickups.splice(i,1);
                }
            }
        }

        function spawnPickup(xPos,yPos,rad) {
            var pickup = {
                id: server.lastSpriteID++,
                parent: null,
                new: true,
                type: randomInt(3,8),
                radius: 15*rad,
                x: xPos,
                y: yPos,
                scale: rad,
                rot: Math.random()*6.0,
                dx: randomInt(-20,20)/100,
                dy: randomInt(-20,20)/100,
                toRemove: false
            };
            pickups.push(pickup);
        }

        // Returns all projectiles
        function getAllProjectiles() {
            return projectiles;
        }

        // Removes instances of projectiles server side, doesn't affect
        // the client's listing object.
        // Justin Tennant 4.04.18
        function cleanProjectiles() {
            for (var i = 0; i < projectiles.length; i++) {
                if (projectiles[i].toRemove === true) {
                    projectiles.splice(i, 1);
                }
            }
        }

        // Return random int between low and high
        // -Justin Tennant, 2-16-18
        function randomInt(low, high) {
            return Math.floor(Math.random() * (high - low) + low);
        }
    }
};

// This line is a node specific line needed to export the contents of the file. Follow this link for more:
//      https://evdokimovm.github.io/javascript/nodejs/2016/06/13/NodeJS-How-to-Use-Functions-from-Another-File-using-module-exports.html
exports.data = gameSetup;

