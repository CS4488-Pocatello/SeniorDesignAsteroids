// Brandon Biggs 20180227
// NODE FILE
// I exported this file from the main index.js to here so index.js was cleaned up a little bit. The code
//      has not changed, except now there is a variable function which accepts io as input from index.js

var chatSetup = {
    setupChat: function (io) {
        // Chatroom
        var numUsers = 0;

        io.on('connection', function (socket) {
            var addedUser = false;

            // when the client emits 'new message', this listens and executes
            socket.on('new message', function (data) {
                // we tell the client to execute 'new message'
                socket.broadcast.emit('new message', {
                    username: socket.username,
                    message: data
                });
            });

            // when the client emits 'add user', this listens and executes
            socket.on('add user', function (username) {
                if (addedUser) return;

                // we store the username in the socket session for this client
                socket.username = username;

                ++numUsers;
                addedUser = true;
                socket.emit('login', {
                    numUsers: numUsers
                });
                // echo globally (all clients) that a person has connected
                socket.broadcast.emit('user joined', {
                    username: socket.username,
                    numUsers: numUsers
                });
            });

            // when the client emits 'typing', we broadcast it to others
            socket.on('typing', function () {
                socket.broadcast.emit('typing', {
                    username: socket.username
                });
            });

            // when the client emits 'stop typing', we broadcast it to others
            socket.on('stop typing', function () {
                socket.broadcast.emit('stop typing', {
                    username: socket.username
                });
            });

            // when the user disconnects.. perform this
            socket.on('disconnect', function () {
                if (addedUser) {
                    --numUsers;

                    // echo globally that this client has left
                    socket.broadcast.emit('user left', {
                        username: socket.username,
                        numUsers: numUsers
                    });
                }
            });


        });
    }
};

// This line is a node specific line needed to export the contents of the file. Follow this link for more:
//      https://evdokimovm.github.io/javascript/nodejs/2016/06/13/NodeJS-How-to-Use-Functions-from-Another-File-using-module-exports.html
exports.data = chatSetup;

