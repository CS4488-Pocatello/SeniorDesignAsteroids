// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 1337;


server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(path.join(__dirname, 'common')));

// BB - This variable references the socket chat file and allows the functions to be referenced.
var chat = require('./Server/socketChat.js');

// BB - This is a specific way to call the setup chat function in the socketChat file.
chat.data.setupChat(io);

// Sprite ID, starts at 0.
server.lastSpriteID = 0;

// BB - This variable references the game setup file
var game = require('./Server/socketGame.js');

// BB - This is a specific way to call the game setup function
game.data.setupGame(io, server);



