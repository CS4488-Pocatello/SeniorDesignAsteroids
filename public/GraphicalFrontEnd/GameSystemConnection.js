"use strict";
/*
 * Jacob Lehmer
 * 2018-01-31
 * This is a series of methods that are meant for server initalization
 * This is the graphical systems connection to the outside world
 */

// Kludge to make sure camera centers when player loads in - Justin T. 3-9-18
var firstWorldLoad = true;
var keycodesNSuchSent = []; //This is the mockup for sending to the server
var keycodesNSuchRecieved = []; //This is the mockup for recieving from the server

//JPL2018-04-14 This is an explosion that will be swapped when something is being removed

//This will be initially responsible for setting up all of the proper structures
//This needs to get some data from the server for the player ID and such
function initializeSystem() {
    world = new gameWorld(0, 0, 50, 50);
    listing = new spriteListing();
    //This is just a place holder, this needs to come from the server... somehow
    //var rock = document.getElementById("rock");

    // Justin Tennant - Moved player creation to two seperate functions handled
    // by server and client files. (loadInPlayer() and addNewPlayer())

    // Justin Tennant - Kludge, changed sprite id's of rocks to higher numbers as to
    // not interfere with player ID's. This needs to be handled differently later.
    //listing.addElement(new sprite(rock,200,25,0,0,.1));
    //listing.addElement(new sprite(rock,201,-25,0,0,.1));

};

// Load player ID and originating X Y when server is ready
// This is called automatically after GraphicsControl.js calls
// Client.askNewPlayer() in the onload function.
// - Justin Tennant 2.26.18
function loadInPlayer(id, x, y) {
    var image = document.getElementById("texImage");
    var anim = document.getElementById("ThrustAnimation");
    var damanim = document.getElementById("DamageAnimation");


    // This is the actual player for the current client instance
    player = new Player(id, null, 100);

    listing.addElement(new sprite(image, player.ID, x, y, 0, .1));
    listing.getElement(player.ID).animation = new animation(anim, 4, 5, 0, -.7, false);
    listing.getElement(player.ID).damageAnimation = new animation(damanim, 4, 2, 0, 0, false);

}

// Calls whenever any new client connects, places them on screen at their
// originating location
// - Justin Tennant 2.20.18
function addNewPlayer(id, x, y, username, health) {
    // if statement to ignore command when server sends client's own ID (speedup)
    if (id != player.ID) {
        var image = document.getElementById("texImage");
        var anim = document.getElementById("ThrustAnimation");

        // This is for other players in listing
        var newPlayer = new Player(id, username, health);

        listing.addElement(new sprite(image, newPlayer.ID, x, y, 0, .1));
        listing.getElement(newPlayer.ID).animation = new animation(anim, 4, 5, 0, -.7, false);
    }
}

// TEMP FUNCTION: 
// Calls this client connects, places all old projectiles on screen
// originating location
// - Justin Tennant 3.11.18
function addNewSprite(id, x, y, rot) {
    // if statement to ignore command when server sends client's own ID (speedup)
    var image = document.getElementById("Projectile");
    var anim = document.getElementById("ProjectileEffects");

    // This is for other players in listing
    var newSprite = new sprite(image, id, x, y, rot, .02);
    newSprite.animation = new animation(anim, 4, 2, 0, 0, true);

    listing.addElement(newSprite);
}

// Called by server when player id disconnects.
// This maybe should be integrated with one of Jake's existing structures
// - Justin Tennant 3.2.18
function removePlayer(id) {
    listing.removeElement(id);
}

//This will send updateData on sync... well eventually anyway
function sendStroke(thrusting, dRot) {
    var cde = new clientToServerSendCode();

    // Send updated thrust rot, animation to server on input
    // - Justin Tennant 2.28.18
    if (thrusting != 0 || dRot != 0) {
        cde.SpriteID = player.ID;
        cde.thrusting = thrusting;
        cde.dRot = dRot;
        cde.fire = false;
        keycodesNSuchSent.push(cde);
        Client.sendMovements();
    }
    // Kludge to trick world to center on screen when player loads in
    else if (firstWorldLoad === true) {
        // never do this again
        firstWorldLoad = false;
        // make cde = player in listing
        cde = listing.getElement(player.ID);
        // add extra SpriteID to match receiving code from server
        cde.SpriteID = player.ID;
        // Fake a server push with unchanged coordinates
        keycodesNSuchRecieved.push(cde);
        // Force events update to center player
        getEvents();
    }
};

//this will shoot
function shoot() {
    var cde = new clientToServerSendCode();
    cde.SpriteID = player.ID;
    cde.thrusting = 0;
    cde.dRot = 0;
    cde.fire = true;
    keycodesNSuchSent.push(cde);
    Client.sendMovements();
}

//This will recieve updateData on sync... well eventually anyway
function getEvents() {
    //Do some connection stuff here in theory anyway
    //This is where data is gotten from the server not sent to it
    if (keycodesNSuchRecieved.length === 0) return;
    keycodesNSuchSent.length = 0;
    var cde = keycodesNSuchRecieved.shift();
    keycodesNSuchRecieved.length = 0;

    //Past this point is code to keep

    if (cde.remove === true) {
        listing.removeElement(cde.SpriteID);
        return;
    }

    if (cde.SpriteID === player.ID) {
        var obj = listing.getElement(player.ID);

        //special things, like moving the camera
        if (cde.X !== -1) {
            world.CenterX = cde.X;
            obj.X = cde.X;
        }
        if (cde.Y !== -1) {
            world.CenterY = cde.Y;
            obj.Y = cde.Y;
        }
        if (cde.Rot !== -1) {
            obj.Rot = cde.Rot;
        }
    }
    else {
        if (cde.new === false) {
            var obj = listing.getElement(cde.SpriteID);

            if (cde.X !== -1) {
                obj.X = cde.X;
            }
            if (cde.Y !== -1) {
                obj.Y = cde.Y;
            }
            if (cde.Rot !== -1) {
                obj.Rot = cde.Rot;
            }
        }
        else {
            switch (cde.type) {
                case 3: //once again 3 is projectile
                    var sprt = new sprite(document.getElementById("Projectile"), cde.SpriteID, cde.X, cde.Y, cde.Rot, .02);
                    sprt.animation = new animation(document.getElementById("ProjectileEffects"), 4, 2, 0, 0, true);
                    listing.addElement(sprt);
                    break;
            }
            //listing.getElement(player.ID).animation = new animation(anim,4,5,0,-.7,false);
        }
    }

    //JPL 2018-03-03 Made kludge to keep server from erroring
    var obj = listing.getElement(player.ID);
};

function createNewProjectile(obj) {
    var newShot = new sprite(document.getElementById("Projectile"), obj.id, obj.x, obj.y, obj.rot, .02);
    newShot.animation = new animation(document.getElementById("ProjectileEffects"), 4, 2, 0, 0, true);
    listing.addElement(newShot);
}

// Creates a new asteroid
function createAsteroid(obj) {
    var newAst = new sprite(document.getElementById("rock"), obj.id, obj.x, obj.y, obj.rot, obj.scale);
    listing.addElement(newAst);
}

// Creates a new pickup
// Andrew Perot 4/12/18
function createPickup(obj){
    if(obj.type == 4) {
        var newPickup = new sprite(document.getElementById("health"), obj.id, obj.x, obj.y, obj.rot, obj.scale);
        listing.addElement(newPickup);
    }
    if(obj.type == 5){
        var newPickup = new sprite(document.getElementById("invulnerability"), obj.id, obj.x, obj.y, obj.rot, obj.scale);
        listing.addElement(newPickup);
    }
    if(obj.type == 6){
        var newPickup = new sprite(document.getElementById("gGun"), obj.id, obj.x, obj.y, obj.rot, obj.scale);
        listing.addElement(newPickup);
    }
    if(obj.type == 7){
        var newPickup = new sprite(document.getElementById("jump"), obj.id, obj.x, obj.y, obj.rot, obj.scale);
        listing.addElement(newPickup);
    }
}
