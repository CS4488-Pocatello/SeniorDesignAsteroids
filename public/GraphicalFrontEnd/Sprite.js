/*
 * Jacob Lehmer
 * 2018-01-31
 * This will be the basic element for the sprite
 */

var sprite = function(img,ID,x,y,rot,scale)
{
  this.ID = ID;
  this.texture = configureTexture(img);
  this.animation = null;
  this.X = x;
  this.Y = y;
  this.Rot = rot;
  this.scale = scale;
  this.damageAnimation = null;

};


