/*
 * Jacob Lehmer 2018-01-31
 * This will contain the required information about the player
 
 * Justin Tennant - Updated on 2018-02-23 with X, Y coordinates
 * May be temporary.
 * Justin Tennant - Updated on 2018-04-11 - Removed X, Y, ROT.
 * Added username
 */
var Player = function(ID, username, health)
{
  this.ID = ID;
  this.username = username;
  this.health = health;
};

