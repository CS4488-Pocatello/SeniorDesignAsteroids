"use strict";
/* Jacob Lehmer
 * 2018-01-29
 * This is the controller for handling the html graphics page
 */
var canvas;
var gl;
var rect;
var spriteRectangle;
var playerTexture;
var zoomDustTexture;
var program;
var animprogram;
var aspect;
var allowedToNavigate = true;
var allowedToShoot = true;

var world;
var player;
var listing;

var startx;
var starty;
var leftMouseDown;

var serverSendTimeoutSet;
var serverSendTimeout;
var visSpot;

var wDown;
var aDown;
var sDown;
var dDown;
var shiftDown;
var fuel = 100;
var rotation = 0;
var thrusting = 0;

//Sounds - Andrew Spooner
var laser;
var thruster;
var playerDeath;
var asteroidBreak;

/*
Sounds currently not used
var outOfBounds;
*/

//This is the standard drawing routine
function tick() {
    if (player != null) {
        if (dDown) rotation -= Math.PI / 18;
        if (aDown) rotation += Math.PI / 18;
        if (wDown) {
            thrusting += .1;
            listing.getElement(player.ID).animation.run();
            thruster.play();
            Client.thrustOn();
            if (thruster.currentTime >= thruster.duration * 0.75)
                thruster.currentTime = 0;
        }
        else {
            listing.getElement(player.ID).animation.stop();
            Client.thrustOff();
        }
        if (serverSendTimeout <= 0) {
            sendStroke(thrusting, rotation);
            serverSendTimeout = serverSendTimeoutSet;
            rotation = 0;
            thrusting = 0;
        }
        //Ryan Rickenbach
        //Added a boost function while holding shift this works on a fuel system
        //Fuel regens when not boosting.
        //Added this to see if people look at code if not I will win when this is demoed in class
        //If you are reading this good job shhhhh.
        if (shiftDown) {
            if (fuel > 0) {
                fuel -= 2;
                //thrusting += .05;
            }
        }
        if (!shiftDown) {
            if (fuel < 100) {
                fuel++;
            }
        }

        else {
            serverSendTimeout--;
        }
    }

    getEvents();
    render();
    setTimeout(function () {
        requestAnimFrame(tick);
    }, 1000.0 / 40.0, 0);

}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    listing.draw();
}

//This will set the current Texture for the system
//Ideally this will change with the sprite being loaded
function setTex(texture, thisProgram) {
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);

    gl.uniform1i(gl.getUniformLocation(thisProgram.program, "texture"), 0);
};

//This will actually render the rectanlge to the screen
function renderRectangle(ThisProgram) {
    gl.useProgram(ThisProgram.program);

    gl.enableVertexAttribArray(ThisProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, spriteRectangle.vBuffer);
    gl.vertexAttribPointer(ThisProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(ThisProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, spriteRectangle.cBuffer);
    gl.vertexAttribPointer(ThisProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(ThisProgram.texCoordLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, spriteRectangle.tBuffer);
    gl.vertexAttribPointer(ThisProgram.texCoordLoc, 2, gl.FLOAT, false, 0, 0);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, spriteRectangle.numVertices - 1);
}

//This will render a dot!
function renderDot(ThisProgram) {
    gl.useProgram(ThisProgram.program);

    gl.enableVertexAttribArray(ThisProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, spriteRectangle.vBuffer);
    gl.vertexAttribPointer(ThisProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(ThisProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, spriteRectangle.cBuffer);
    gl.vertexAttribPointer(ThisProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(ThisProgram.texCoordLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, spriteRectangle.tBuffer);
    gl.vertexAttribPointer(ThisProgram.texCoordLoc, 2, gl.FLOAT, false, 0, 0);

    gl.drawArrays(gl.POINT, spriteRectangle.numVertices - 1, 1);
}

//This is the initalization for the grahpics system 
window.onload = function init() {

    // Sets background music volume - Andrew Spooner
    var bgMusic = document.getElementById('bgMusic');
    bgMusic.volume = 0.1;

    // Initializes sounds - Andrew Spooner
    laser = document.getElementById('laser');
    laser.volume = 0.10;
    thruster = document.getElementById('thruster');
    thruster.volume = 0.10;
    playerDeath = document.getElementById('playerDeath');
    playerDeath.volume = 0.30;
    asteroidBreak = document.getElementById('asteroidBreak');
    asteroidBreak.volume = 0.15;
    /*
    Sounds currently not used
    outOfBounds = document.getElementById('outOfBounds');
    */

    document.onkeydown = keyDown;
    document.onkeyup = keyUp;

    serverSendTimeoutSet = 0;
    serverSendTimeout = serverSendTimeoutSet;
    canvas = document.getElementById("gl-canvas");
    rect = canvas.getBoundingClientRect();

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn't available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);

    aspect = canvas.width / canvas.height;

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

    spriteRectangle = new SpriteRectangle();

    program = new SimpleProgram(gl);
    animprogram = new AnimProgram(gl);
    zoomDustTexture = configureTexture(document.getElementById("Dust"));
    visSpot = configureTexture(document.getElementById("VisSpot"));

    // Need ID, and starting X, Y coordinates. 0 placeholder
    // - Justin Tennant 2-23-18
    initializeSystem();

    // Tells client file to ask server for a new player. Server should
    // Create a new player, assign an ID to this client, and let other clients
    // know that new player has arrived.
    // - Justin Tennant 2.28.18
    Client.askNewPlayer();

    tick();

    Client.initializeServer();
};

function keyDown(e) {
    //Edited by Ryan Rickenbach 2018-04-10 code to block keyboard input when chat window is active. This can break if you were to hold CTRL.
    if (e.keyCode === 17 && allowedToNavigate == true) {
        allowedToNavigate = false;
    }
    else if (e.keyCode === 17 && allowedToNavigate == false) {
        allowedToNavigate = true;
    }
    if (allowedToNavigate) {
        switch (e.keyCode) {
            case 87:
                // w
                wDown = true;
                break;
            case 38:
                wDown = true;
                break
            case 65:
                // a
                aDown = true;
                break;
            case 37:
                aDown = true;
                break;
            case 83:
                // s
                sDown = true;
                break;
            case 40:
                sDown = true;
                break;
            case 68:
                // d
                dDown = true;
                break;
            case 39:
                dDown = true;
                break;
            case 32:
                // spacebar
                //Edited by Ryan Rickenbach 2018-04-10 code used to make only fire once per space bar.
                if (allowedToShoot) {
                    shoot();
                    laser.currentTime = 0;
                    laser.play();
                    allowedToShoot = false;
                }
                break;
            case 16:
                shiftDown = true;
                break;
            default:
                //console.log("Unrecognized key press: " + e.keyCode);
                break;
        }
    }
}


function keyUp(e) {
    switch (e.keyCode) {
        case 87:
            // w
            wDown = false;
            thruster.pause();
            Client.thrustOff();
            break;
        case 38:
            wDown = false;
            thruster.pause();
            Client.thrustOff();
            break;
        case 65:
            // a
            aDown = false;
            break;
        case 37:
            aDown = false;
            break;
        case 83:
            // s
            sDown = false;
            break;
        case 40:
            sDown = false;
            break;
        case 68:
            // d
            dDown = false;
            break;
        case 39:
            dDown = false;
            break;
        //Edited by Ryan Rickenbach 2018-04-10 Allow to fire again when key is raised.
        case 32:
            allowedToShoot = true;
            break;
        //See boost comment at top of page
        case 16:
            shiftDown = false;
        default:
            //console.log("Unrecognized key press: " + e.keyCode);
            break;
    }
}

function configureTexture(image) {
    var texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,
        gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
        gl.NEAREST_MIPMAP_LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    return texture;
}

var SimpleProgram = function () {
    this.program = initShaders(gl, "vshader", "fshader");

    gl.useProgram(this.program);
    this.vertexLoc = gl.getAttribLocation(this.program, "vPosition");
    this.colorLoc = gl.getAttribLocation(this.program, "vColor");
    this.texCoordLoc = gl.getAttribLocation(this.program, "vTexCoord");

    this.mvMatrixLoc = gl.getUniformLocation(this.program, "mvMatrix");
};

var AnimProgram = function () {
    this.program = initShaders(gl, "vshader", "animFshader");

    gl.useProgram(this.program);
    this.vertexLoc = gl.getAttribLocation(this.program, "vPosition");
    this.colorLoc = gl.getAttribLocation(this.program, "vColor");
    this.texCoordLoc = gl.getAttribLocation(this.program, "vTexCoord");
    this.stageLoc = gl.getUniformLocation(this.program, "stage");
    this.stageCountLoc = gl.getUniformLocation(this.program, "stageCount");

    this.mvMatrixLoc = gl.getUniformLocation(this.program, "mvMatrix");
};

function degrees(radians) {
    return radians * 180.0 / Math.PI;
}