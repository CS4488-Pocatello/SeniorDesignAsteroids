"use strict";
//Jacob Lehmer
//2018-01-28
//This is the object that is responsible for holding the square

var SpriteRectangle = function()
{
    this.vertices = [
    vec4(-0.5, -0.5,  0.0, 1.0),
    vec4( .50, -0.5,  0.0, 1.0),
    vec4(-0.5, 0.5,  0.0, 1.0),
    vec4( 0.5,  .50,  0.0, 1.0),
    vec4( 0.0,  0.0,  0.0, 1.0)
  ];

  this.numVertices = this.vertices.length;

  this.vertexColors = [
    vec4(1.0, 1.0, 1.0, 1.0),  // white
    vec4(1.0, 0.0, 0.0, 1.0),  // red
    vec4(0.0, 1.0, 0.0, 1.0),  // green
    vec4(0.0, 0.0, 1.0, 1.0),  // blue
    vec4(1.0,1.0,0.0,0.0)
  ];

  this.texCoords = [
    vec2(0, 0),
    vec2(1, 0),
    vec2(0, 1),
    vec2(1, 1),
    vec2(.5,.5)
  ];
  
  this.vBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, this.vBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(this.vertices), gl.STATIC_DRAW);

  this.cBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, this.cBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(this.vertexColors), gl.STATIC_DRAW);

  this.tBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, this.tBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(this.texCoords), gl.STATIC_DRAW);
};