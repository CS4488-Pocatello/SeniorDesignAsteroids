"use strict";
/* Jaccb Lehmer
 * 2018-01-31
 * This represents the area of the game world that the canvas actually sits in
 */

var gameWorld = function(cntrx,cntry,w,h)
{
  this.CenterX = cntrx;
  this.CenterY = cntry;
  this.Width = w;
  this.Height = h;
};


