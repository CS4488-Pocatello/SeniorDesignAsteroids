"use strict";
/* Jacob Lehmer
 * 2018-01-31
 * This is the data structure that will handle the storage of 
 * all of the different sprite objects
 */

var spriteListing = function()
{

 this.list = [];
 this.zoomDusts = [];
 
 for(var i =0;i< 20; i++)
 {
   this.zoomDusts.push(new zoomDust(world.CenterX + (Math.random()-.5)*2*world.Width,world.CenterY + (Math.random()-.5)*2*world.Height));
 }
 
 this.addElement = function(elmnt)
 {
   this.list.push(elmnt);
 };
 
  
 this.getElement = function(ID)
 {
   for(var i =0;i<this.list.length;i++)
   {
     if(ID === this.list[i].ID) return this.list[i];
   }
 };
 
 this.removeElement = function(ID)
 {
     //console.log("I am removing element of ID: " + ID);
    for(var i =0;i<this.list.length;i++)
   {
     if(ID === this.list[i].ID) {
         //console.log("The remove statement fired at index: " + i);
         //this.list.splice(i, 1);
         this.list[i].animation = new animation(document.getElementById("ExplosionAnimation"), 4, 4, 0, 0, true);
         this.list[i].animation.removeOnFinish = true;
     }
   }

 };
 
 this.drawZoomDust = function()
 {
   gl.useProgram(program.program);
   setTex(zoomDustTexture,program);

   for(var i =0;i<this.zoomDusts.length;i++)
   {
     var obj = this.zoomDusts[i];

     if(Math.abs(obj.X-world.CenterX)>(world.Width/2) && Math.abs(obj.Y-world.CenterY)>(world.Height/2) )
     {
       var axis = (Math.random()-.5);
       var side = (Math.random()-.5);
       if(axis > 0) //Axis greater than zero means spawn dot Max or Min Y value
       {
         if(side > 0)//Side Greater than zero means spawn Max Y value
         {
           obj.Y = world.CenterY + world.Height;
           obj.X = world.CenterX + (Math.random()-.5)*world.Width;
         }
         else
         {
           obj.Y = world.CenterY - world.Height;
           obj.X = world.CenterX + (Math.random()-.5)*world.Width;
         }
       }
       else
       {
        if(side > 0)//Side Greater than zero means spawn Max X value
         {
           obj.X = world.CenterX + world.Width;
           obj.Y = world.CenterY + (Math.random()-.5)*world.Height;
         }
         else
         {
           obj.X = world.CenterX - world.Width;
           obj.Y = world.CenterY + (Math.random()-.5)*world.Height;
         }
       }
     }
      var mvMatrix = translate((obj.X-world.CenterX)/world.Width,(obj.Y-world.CenterY)/world.Height,0);
      mvMatrix = mult(mvMatrix,scalem(.02,.02,.02));
      gl.uniformMatrix4fv(program.mvMatrixLoc,false,flatten(mvMatrix));
      renderRectangle(program);
   }
   
 };
 
 this.draw = function()
 { 
   this.drawZoomDust();
   for(var i =0;i<this.list.length;i++)
   {
     var obj = this.list[i];
     var mvMatrix = translate((obj.X-world.CenterX)/world.Width,(obj.Y-world.CenterY)/world.Height,0);
     mvMatrix = mult(mvMatrix,scalem(obj.scale,obj.scale,obj.scale));
     mvMatrix = mult(mvMatrix,rotateZ(degrees(obj.Rot)-90));

     gl.useProgram(program.program);
     gl.uniformMatrix4fv(program.mvMatrixLoc,false,flatten(mvMatrix));

     setTex(obj.texture,program);
     renderRectangle(program);

     if(obj.animation !== null && obj.animation.running)
     {
       if(obj.animation.removeOnFinish && obj.animation.frameState === obj.animation.frameCount)
       {
          this.list.splice(i, 1);
       }
        var offSet = mult(mvMatrix,translate(obj.animation.offsetX,obj.animation.offsetY,0));
        gl.useProgram(animprogram.program);
        gl.uniformMatrix4fv(animprogram.mvMatrixLoc,false,flatten(offSet));
        gl.uniform1i(animprogram.stageCountLoc,obj.animation.frameCount);
        gl.uniform1i(animprogram.stageLoc,obj.animation.frameState);
        setTex(obj.animation.rawFrames,animprogram);
        renderRectangle(animprogram);
        obj.animation.increment();
     }

     if(obj.damageAnimation !== null && obj.damageAnimation.running)
     {
        var offSet = mult(mvMatrix,translate(obj.damageAnimation.offsetX,obj.damageAnimation.offsetY,0));
        gl.useProgram(animprogram.program);
        gl.uniformMatrix4fv(animprogram.mvMatrixLoc,false,flatten(offSet));
        gl.uniform1i(animprogram.stageCountLoc,obj.damageAnimation.frameCount);
        gl.uniform1i(animprogram.stageLoc,obj.damageAnimation.frameState);
        setTex(obj.damageAnimation.rawFrames,animprogram);
        renderRectangle(animprogram);
        obj.damageAnimation.increment();
     }
     var mvMatrix = translate((obj.X-world.CenterX)/(world.Width*4),(obj.Y-world.CenterY)/(world.Height*4),0);
     gl.clearColor(.2,.2,.2,.5);

     gl.uniformMatrix4fv(program.mvMatrixLoc,false,flatten(mvMatrix));
     gl.viewport(canvas.width*(3.0/4.0)-50, 25, canvas.width*(1.0/4.0), canvas.height*(1.0/4.0));
     renderDot(program); 
     gl.viewport(0, 0, canvas.width, canvas.height);
     gl.clearColor(0,0,0,1);

   }
    // gl.viewport(canvas.width*(3.0/4.0)-50, 25, canvas.width*(1.0/4.0), canvas.height*(1.0/4.0));
    // setTex(visSpot,program);
    // gl.uniformMatrix4fv(program.mvMatrixLoc,false,flatten(scalem(2,2,2)));
    // renderRectangle(program);
    // gl.viewport(0, 0, canvas.width, canvas.height);

 };

};

