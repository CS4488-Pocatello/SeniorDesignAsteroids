/* 
 * Jacob Lehmer
 * 2018-03-01
 * This is the structure for the animations
 */
var animation = function(image, frameCount,timerCounter,offx,offy,running)
{
  this.rawFrames = configureTexture(image);
  this.frameCount = frameCount;
  this.frameState = frameCount;
  this.timerCounterSet=timerCounter;
  this.timerCounter = 0;
  this.offsetX = offx;
  this.offsetY = offy;
  this.running = running;
  this.removeOnFinish = false;
  
  this.run = function()
  {
    this.running = true;
  };
  
  this.stop = function()
  {
    this.running = false;
    this.frameState = 0;
    this.timerCounter = 0;
  };
  
  this.increment = function()
  {
    this.timerCounter--;
    if(this.timerCounter <=0)
    {
      this.timerCounter = this.timerCounterSet;
      this.frameState = (this.frameState + 1)%this.frameCount;
    }
  };
};


