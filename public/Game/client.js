// Local client file, repsonsible for interfacing game with server
// GraphicsControl will call functions from here on client request, 
// and this will call functions on GameSystemConnection on server
// request. 
// - Justin Tennant
var Client = {};
Client.socket = io.connect();

// Lets server know new player connected, server will assign ID
// and random starting position
Client.askNewPlayer = function () {
    Client.socket.emit('newplayer');
};

// non-functioning Placeholder to send server player actions
Client.sendMovements = function () {
    var cde = keycodesNSuchSent.shift();
    var thrusting = cde.thrusting;
    var rot = cde.dRot;
    var fire = cde.fire;
    Client.socket.emit('input', {thrusting: thrusting, rot: rot, fire: fire, turbo: shiftDown});
};

Client.thrustOn = function () {
    Client.socket.emit('thrust', true);
}

Client.thrustOff = function () {
    Client.socket.emit('thrust', false);
}

// One time call, server assigns this client's player ID
Client.socket.on('thisplayer', function (data) {
    loadInPlayer(data.id, data.x, data.y);
});


// Server updates this client with new player data on each new client 
// connect
Client.socket.on('newplayer', function (data) {
    addNewPlayer(data.id, data.x, data.y, data.username, data.health);
});

// Temp function, not used right now
//Client.socket.on('projectiles', function(data){
//  for(var i = 0; i < data.length; i++){
//    addNewSprite(data[i].id,data[i].x,data[i].y, data[i].rot);
//  }
//});

// Don't inititate any of these sockets until game is loaded
Client.initializeServer = function () {

    // Client receives position data for other players from server on tick
    Client.socket.on('updatePlayers', function (data) {
        if (listing != null)
            for (var i = 0; i < data.length; i++) {
                var obj = listing.getElement(data[i].id);
                if (!obj)
                    continue;

                // Speedup: If the value in listing is different, update it with the
                // server value, else check the next ID right away.
                if (obj.X != data[i].x || obj.Y != data[i].y || obj.Rot != data[i].rot
                    || obj.animation.running != data[i].animation) {

                    // This code would update the players in the listing via getEvents,
                    // but it makes the game feel choppy. Only really lets 1 player move
                    // smoothly at a time
                    /*
                    var recCode = new controlCode();
                    recCode.SpriteID = data[i].id;
                    recCode.new = data[i].new;
                    recCode.type = data[i].type;
                    recCode.remove = data[i].remove;
                    recCode.X = data[i].x;
                    recCode.Y = data[i].y;
                    recCode.Rot = data[i].rot;
                    keycodesNSuchRecieved.push(recCode);
                    */

                    //*************************************************************************
                    // Temporary getEvents bypass for all but this client until I understand it better
                    if (data[i].id === player.ID) {
                        // This uses get events if player so it can still center on screen
                        var recCode = new controlCode();
                        recCode.SpriteID = data[i].id;
                        recCode.new = data[i].new;
                        recCode.type = data[i].type;
                        recCode.remove = data[i].remove;
                        recCode.X = data[i].x;
                        recCode.Y = data[i].y;
                        recCode.Rot = data[i].rot;
                        //console.log(data[i].rot);
                        keycodesNSuchRecieved.push(recCode);
                    }
                    else {
                        // This directly edits the listing
                        obj.X = data[i].x;
                        obj.Y = data[i].y;
                        obj.Rot = data[i].rot;
                        if (data[i].animation === true) {
                            obj.animation.run();
                        }
                        else {
                            obj.animation.stop();
                        }
                    }
                    //*************************************************************************
                }
            }
    });

    //var firstLog = true;

    Client.socket.on('removeAsteroid', function (data) {
        //Plays asteroidBreak sound on asteroid removal - Andrew Spooner
        var asteroidBreak = document.getElementById('asteroidBreak');
        asteroidBreak.play();
        
        listing.removeElement(data);
    });
    
    // When player dies, they are already removed from server, but this
    // function also removes them from the graphics listing so they 
    // dissapear, unless its this player.
    // - Justin Tennant, 4.22.18
    Client.socket.on('player died', function (id) {
        if (id != player.ID)
            listing.removeElement(id);
    });

    Client.socket.on('updateProjectiles', function (data) {
        //console.log(data);
        if (listing != null && listing != undefined) {
            for (var i = 0; i < data.length; i++) {
                var obj = listing.getElement(data[i].id);
                if (obj != null && obj != undefined) {
                    if (data[i].toRemove === false) {
                        obj.X = data[i].x;
                        obj.Y = data[i].y;
                    }
                    else {
                        listing.removeElement(data[i].id);
                    }

                    // TJ's old testing code for projectile aging
                    //}
                    // else{
                    //     if(firstLog) {
                    //         firstLog = false;
                    //         //console.log(data)
                    //         console.log("I should be removed! My id is: " + data[i].id);
                    //         console.log("Before");
                    //         console.log(listing.list);
                    //         listing.removeElement(data[i].id);
                    //         console.log("After");
                    //         console.log(listing.list);
                    //     }
                    //     else {
                    //         //listing.removeElement(data.id);
                    //     }
                    //
                    // }
                }
                else {
                    createNewProjectile(data[i]);
                }
            }
        }

    });


    // One time call, server gives coordinates of all existing players when
    // this client connects
    Client.socket.on('allplayers', function (data) {
        for (var i = 0; i < data.length; i++) {
            if (!listing.getElement(data[i].id))
                addNewPlayer(data[i].id, data[i].x, data[i].y, data[i].username, data[i].health);
        }

        // Client receives request from server to remove a player
        Client.socket.on('remove', function (id) {
            removePlayer(id);
        });

    });

    // Server updates all asteroids
    Client.socket.on('updateAsteroids', function (data) {
        if (listing != null && listing != undefined) {
            for (var i = 0; i < data.length; i++) {
                var obj = listing.getElement(data[i].id);
                if (obj != null && obj != undefined) {
                    obj.X = data[i].x;
                    obj.Y = data[i].y;
                }
                else {
                    createAsteroid(data[i]);
                }
            }
        }
    });
    
    // Server updates all pickups
    // Andrew Perot 4/12/18
    Client.socket.on('updatePickups',function(data){
        if(listing != null && listing != undefined){
            for(var i=0; i<data.length; i++){
                var obj = listing.getElement(data[i].id);
                if(obj != null && obj != undefined){
                    obj.X = data[i].x;
                    obj.Y = data[i].y;
                }
                else{
                    createPickup(data[i]);
                }
            }
        }
    });

    Client.socket.on('removePickup',function(data){
        listing.removeElement(data);
    });
}