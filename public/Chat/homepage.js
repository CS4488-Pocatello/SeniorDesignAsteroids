// Brandon Biggs - 20180227
// My thoughts for this file could be for where all of the scripts go that make the front page work.
//      Ryan an JD worked on the home page and I wanted to move the scripts in to here instead
//      of leaving them in index.html

function showCredits() {
    var $loginPage = $('.login.page')
    var $creditsPage = $('.credits.page')
    $loginPage.fadeOut();
    $creditsPage.fadeIn();
}

function showInstructions() {
    var $loginPage = $('.login.page')
    var $instructionsPage = $('.instructions.page')
    var $instructionsReturn = $('.drawR')
    if ($loginPage.is(':visible')) {
        $instructionsReturn.fadeIn();
        $loginPage.fadeOut();
        $instructionsPage.fadeIn();
    }
}

function hideCredits() {
    var $loginPage = $('.login.page')
    var $creditsPage = $('.credits.page')
    $creditsPage.fadeOut();
    $loginPage.fadeIn();
}

function hideInstructions() {
    var $loginPage = $('.login.page')
    var $instructionsPage = $('.instructions.page')
    var $instructionsReturn = $('.drawR')
    $instructionsPage.fadeOut();
    $instructionsReturn.fadeOut();
    $loginPage.fadeIn();
}

//Function to restart the game which just removes the game over page socketGame is responsible for player data
function restart() {
    // Revives player on the server when they click restsart
    // - Justin Tennant, 4.22.18
    Client.socket.emit('revivePlayer');
    var $gameOver = $('.gameOver.page')
    $gameOver.fadeOut();
}