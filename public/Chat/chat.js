// BB 20180227 - Notes
//  This was called main.js. I'm now calling it chat.js as it seems like it's entirely just chat related, but I could be wrong.
//Ryan
// No problem with this just being here. This is largely related to chat but this function is also the control function for changing views, this allows for all the
// control screens instructions, chat, credits, login, and the game page itself. A large part of this was taken from Socket Demo and control functions made by Ryan Rickenbach
var scoreArray;
var tempScore = [];

$(function () {
    var FADE_TIME = 150; // ms
    var TYPING_TIMER_LENGTH = 400; // ms
    var COLORS = [
        '#e21400', '#91580f', '#f8a700', '#f78b00',
        '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
        '#3b88eb', '#3824aa', '#a700ff', '#d300e7',
        '#c43bef', '#8010e7', '#12f736', '#49f2ee',
        '#e935d0', '#1437fe', '#df4f33', '#f93253'
    ];

    // Initialize variables
    var $window = $(window);
    var $usernameInput = $('.usernameInput'); // Input for username
    var $messages = $('.messages'); // Messages area
    var $inputMessage = $('.inputMessage'); // Input message input box
    var $scores = $('.scoreArea'); //This is the list of scores
    var $health = $('.healthArea');

    var $loginPage = $('.login.page'); // The login page
    var $chatPage = $('.chat.page'); // The chatroom page
    var $gamePage = $('.game.page').hide(); // The game page - Starts out hidden.
    var $instructionPage = $('.instructions.page').hide(); //The instructions page - Starts hidden
    var $textEntry = $('.inputMessage').hide();
    var $creditsPage = $('.credits.page').hide();
    var $instructionsReturn = $('.buttonReturn').hide();
    var $scorePage = $('.score.page').hide(); // This is the div where scores are displayed
    var $healthPage = $('.health.page').hide();
    var $gameOver = $('.gameOver.page').hide();
    var $healthFlash = $('.healthFlash.page').hide();


    // Prompt for setting a username
    var username;
    var playerID;
    var healthDisplay;
    var connected = false;
    var typing = false;
    var lastTypingTime;
    var $currentInput = $usernameInput.focus();
    var socket = io();


    function addParticipantsMessage(data) {
        var message = '';

        message += "Participants: " + data.numUsers;
        //if (data.numUsers === 1) {
        //    message += "there's 1 participant";
        //} else {
        //    message += "there are " + data.numUsers + " participants";
        //}
        log(message);
    }

    // Sets the client's username
    function setUsername() {

        //Ryan Rickenbach 2018-11-04
        //Tells the server to give client PlayerID
        //KNOWN BUG
        //If users connect nearly at the same time they will hear the same
        //socket on command which will cause two players to have same playerID
        //This will only effect score being non-unique
        Client.socket.emit('get ID');

        if ($usernameInput.val().trim() == "") {
            username = "Player";
        }
        else {
            username = cleanInput($usernameInput.val().trim());
        }
        // If the username is valid
        if (username) {
            $loginPage.fadeOut();
            $gamePage.fadeIn();
            $chatPage.fadeIn();
            $scorePage.fadeIn();
            $loginPage.off('click');
            $healthPage.fadeIn();
            $currentInput = $inputMessage.focus();

            // Tell the chat server your username
            socket.emit('add user', username);

            // Tell the game server your username
            Client.socket.emit('add new user', username);

            // Tell the client your username
            player.username = username;
        }
    }

    // Sends a chat message
    function sendMessage() {
        var message = $inputMessage.val();
        // Prevent markup from being injected into the message
        message = cleanInput(message);
        // if there is a non-empty message and a socket connection
        if (message && connected) {
            $inputMessage.val('');
            addChatMessage({
                username: username,
                message: message
            });
            // tell server to execute 'new message' and send along one parameter
            socket.emit('new message', message);
        }
    }

    // Log a message
    function log(message, options) {
        var $el = $('<li>').addClass('log').text(message);
        addMessageElement($el, options);
    }

    // Adds the visual chat message to the message list
    function addChatMessage(data, options) {
        // Don't fade the message in if there is an 'X was typing'
        var $typingMessages = getTypingMessages(data);
        options = options || {};
        if ($typingMessages.length !== 0) {
            options.fade = false;
            $typingMessages.remove();
        }

        var $usernameDiv = $('<span class="username"/>')
            .text(data.username)
            .css('color', getUsernameColor(data.username));
        var $messageBodyDiv = $('<span class="messageBody">')
            .text(data.message);

        var typingClass = data.typing ? 'typing' : '';
        var $messageDiv = $('<li class="message"/>')
            .data('username', data.username)
            .addClass(typingClass)
            .append($usernameDiv, $messageBodyDiv);

        addMessageElement($messageDiv, options);
    }

    // Adds the visual chat typing message
    function addChatTyping(data) {
        data.typing = true;
        data.message = 'is typing';
        addChatMessage(data);
    }

    // Removes the visual chat typing message
    function removeChatTyping(data) {
        getTypingMessages(data).fadeOut(function () {
            $(this).remove();
        });
    }

    // Adds a message element to the messages and scrolls to the bottom
    // el - The element to add as a message
    // options.fade - If the element should fade-in (default = true)
    // options.prepend - If the element should prepend
    // all other messages (default = false)
    function addMessageElement(el, options) {
        var $el = $(el);

        // Setup default options
        if (!options) {
            options = {};
        }
        if (typeof options.fade === 'undefined') {
            options.fade = true;
        }
        if (typeof options.prepend === 'undefined') {
            options.prepend = false;
        }

        // Apply options
        if (options.fade) {
            $el.hide().fadeIn(FADE_TIME);
        }
        if (options.prepend) {
            $messages.prepend($el);
        } else {
            $messages.append($el);
        }
        //$messages[0].scrollTop = $messages[0].scrollHeight;
        $messages[0].scrollTop += 18;
    }

    // Prevents input from having injected markup
    function cleanInput(input) {
        return $('<div/>').text(input).html();
    }

    // Updates the typing event
    function updateTyping() {
        if (connected) {
            if (!typing) {
                typing = true;
                socket.emit('typing');
            }
            lastTypingTime = (new Date()).getTime();

            setTimeout(function () {
                var typingTimer = (new Date()).getTime();
                var timeDiff = typingTimer - lastTypingTime;
                if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                    socket.emit('stop typing');
                    typing = false;
                }
            }, TYPING_TIMER_LENGTH);
        }
    }

    // Gets the 'X is typing' messages of a user
    function getTypingMessages(data) {
        return $('.typing.message').filter(function (i) {
            return $(this).data('username') === data.username;
        });
    }

    // Gets the color of a username through our hash function
    // Changed to just randomly assign color
    function getUsernameColor(username) {
        // Compute hash code
        var hash = 7;
        for (var i = 0; i < username.length; i++) {
            hash = username.charCodeAt(i) + (hash << 5) - hash;
        }
        // Calculate color
        var index = Math.abs(hash % COLORS.length);
        return COLORS[index];
    }

    //Ryan Rickenbach 2018-11-04
    //Method for updating the HTML for listing scores. This essentially uses a temporary array so it is changed all at once
    //Temp array was needed to allow for sorting and to add "<br>" for html.
    function updateScores() {
        tempScore = []; // clears the array so when a user leaves it is reflected.
        for (i = 0; i < scoreArray.length; i++) {
            tempScore[i] = (scoreArray[i].username + ": " + scoreArray[i].score + "<br>");
        }
        tempScore.toString();
        $scores.html(tempScore);
    }

    //Function to update the HTML to show the scores as it is client specific just has to return the local health value
    //JPL 2018-04-24 Damage animation stuff in a really crappy place
    var oldHealth = healthDisplay;
    var doAnimation = false;
    var animCount = healthDisplay;
    function updateHealth() {
        $health.html("Health: " + healthDisplay);
        
        if(doAnimation)
        {
          animCount --;
        }
        
        if(animCount <= 0)
        {
          listing.getElement(player.ID).damageAnimation.stop();
          doAnimation = false;
        }
      
        if(healthDisplay !== oldHealth)
        {
          doAnimation = true;
          listing.getElement(player.ID).damageAnimation.run();
          animCount = 200/(healthDisplay+1);
          oldHealth = healthDisplay;
        }
    }


    function tellUsername() {
        console.log("Username " + username + " player ID " + playerID);
    }

    // Ryan Rickenbach
    // Keyboard events
    $window.keydown(function (event) {
        // Auto-focus the current input when a key is typed
        if (!(event.ctrlKey || event.metaKey || event.altKey)) {
            $currentInput.focus();
        }
        // When the client hits ENTER on their keyboard
        if (event.which === 13) {
            if (username) {
                sendMessage();
                socket.emit('stop typing');
                typing = false;
            } else {
                setUsername();
            }
        }
        // When the client hits CTRL - Toggles the game and chat page
        if (event.which === 17) {
            if ($textEntry.is(':visible')) {
                $textEntry.fadeOut();
                //$gamePage.fadeIn();
            }
            else {
                //$gamePage.fadeOut();
                $textEntry.fadeIn();
            }

        }
        // When the client hits 'ESC' - Toggle instructions page.
        if (event.which === 27) {
            if ($loginPage.is(':visible') || $instructionsReturn.is(':visible')) {
                //Blank method to do nothing when certain screens are active and ESC is pressed
            }
            else if ($instructionPage.is(':visible')) {
                $instructionPage.fadeOut();
                $gamePage.fadeIn();
                $chatPage.fadeIn()

            }
            else {
                $gamePage.fadeOut();
                $chatPage.fadeOut();
                $instructionPage.fadeIn();
            }
        }
        //Ryan Rickenbach
        //I am keeping this in here for now pressing =
        //will console log your ID. I have been using this to test
        //if two players heard the same ID call which will disrupt health.
        if (event.which === 187) {
            //tellUsername();
            $healthFlash.fadeIn();
            $healthFlash.fadeOut();
        }
    });

    $inputMessage.on('input', function () {
        updateTyping();
    });

    // Click events

    // Focus input when clicking anywhere on login page
    $loginPage.click(function () {
        $currentInput.focus();
    });

    // Focus input when clicking on the message input's border
    $inputMessage.click(function () {
        $inputMessage.focus();
    });

    // Socket events

    //Ryan Rickenbach 2018-11-04
    //Function to call update score and health every 1/10 can be adjusted.
    window.setInterval(function () {
        Client.socket.emit('update scores');
        if (username != null) {
            Client.socket.emit('update health', playerID);
        }
    }, 100);

    // Whenever the server emits 'login', log the login message
    socket.on('login', function (data) {
        connected = true;
        // Display the welcome message
        var message = "Welcome to Asteroids press the 'CTRL' key to enter and exit chat. Press 'ESC' to show instructions"
        log(message, {
            prepend: true
        });
        addParticipantsMessage(data);
    });

    //Ryan Rickenbach 2018-11-04
    //Socket on event to update scores. This pulls information from Socket Game
    //to bring in username and score that is stored on game server.
    socket.on('score updated', function (data) {
        scoreArray = [];
        for (i = 0; i < data.length; i++) {
            scoreArray.push({username: data[i].username, score: data[i].score});
        }
        scoreArray.sort(function (a, b) {
            return b.score - a.score
        }) //sorts the array descending based on score.
        updateScores();
    });

    //Ryan Rickenbach 2018-11-04
    //When server sends back the player score update the player
    socket.on('health updated', function (data) {
        if (data.id == playerID) {
            healthDisplay = data.health;
            updateHealth();
        }
    });

    //Ryan Rickenbach 2018-12-04
    //Server tells client if it died, if so show game over screen
    //Note by Justin Tennant: Wishlist change - Get this to render locally
    //rather than by a socket emit to save network data. Possibly have another
    //flag for when the server tick occurs that if player.isDead call this method
    socket.on('game over', function (data) {
        if (data == playerID) {
            $gameOver.fadeIn();
            
            //Sound on player death - Andrew Spooner
            var playerDeath = document.getElementById('playerDeath');
            playerDeath.play();
        }
    });

    //Ryan Rickenbach 2018-11-04
    //Sets the player ID only if it does not have one.
    socket.on('set playerID', function (data) {
        if (playerID == null) {
            playerID = data;
        }
    });


    // Whenever the server emits 'new message', update the chat body
    socket.on('new message', function (data) {
        addChatMessage(data);
    });

    // Whenever the server emits 'user joined', log it in the chat body
    socket.on('user joined', function (data) {
        log(data.username + ' joined');
        addParticipantsMessage(data);

    });

    // Whenever the server emits 'user left', log it in the chat body
    socket.on('user left', function (data) {
        log(data.username + ' left');
        addParticipantsMessage(data);
        removeChatTyping(data);
    });

    // Whenever the server emits 'typing', show the typing message
    socket.on('typing', function (data) {
        addChatTyping(data);
    });

    // Whenever the server emits 'stop typing', kill the typing message
    socket.on('stop typing', function (data) {
        removeChatTyping(data);
    });

    socket.on('disconnect', function () {
        log('you have been disconnected');
    });

    socket.on('reconnect', function () {
        log('you have been reconnected');
        if (username) {
            socket.emit('add user', username);
        }
    });

    socket.on('reconnect_error', function () {
        log('attempt to reconnect has failed');
    });

});